<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coffee extends Model
{
    protected $table = 'coffees';
    protected $guarded = ['id'];

    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }
}
