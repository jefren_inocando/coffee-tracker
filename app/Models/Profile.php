<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $guarded = ['id'];

    public function coffees()
    {
        return $this->hasMany('App\Models\Coffee');
    }
}
