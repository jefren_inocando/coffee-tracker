<?php

namespace App\Http\Controllers;

use App\Models\Coffee;
use App\Models\Profile;
use Illuminate\Http\Request;

class CoffeeController extends Controller
{
    /**
     * Show Coffees
     *
     * @param  \App\Models\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return view('coffee.show', ['profile' => $profile]);
    }

    /**
     * Creates coffee for a profile
     *
     * @param Profile $profile
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Profile $profile)
    {
        Coffee::create(['profile_id' => $profile->id]);
        return redirect()->route('coffees', ['id' => $profile->id])
            ->with('message', 'Coffee created successfully');
    }
}
