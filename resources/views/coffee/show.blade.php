@extends('layout')

@section('profileLink', route('profiles.update', $profile->id))

@section('coffeeLink', route('coffees', $profile->id))


@section('content')
    <h3>{{ $profile->username }}'s Coffees</h3>

    <ul>
        @foreach($profile->coffees as $coffee)
            <li>{{ date('n/j/y ga', strtotime($coffee->created_at)) }}</li>
        @endforeach
    </ul>

    <a class="btn btn-primary btn-lg" href="{{ route('coffees.create', $profile->id) }}">Add a Coffee</a>

@endsection