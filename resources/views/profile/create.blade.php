@extends('layout')

@section('content')
    <h3>Create Profile</h3>
    {!! Form::open(['method' => 'POST','route' => 'profiles.store']) !!}
    <div class="row">
        <div class="col">
            <div class="form-group">
                <strong>Username:</strong>
                {!! Form::text('username', null, ['placeholder' => 'Username','class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <strong>Full name:</strong>
                {!! Form::text('fullname', null, ['placeholder' => 'Full name','class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg float-right">Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection