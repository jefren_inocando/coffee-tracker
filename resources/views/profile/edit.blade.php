@extends('layout')

@section('profileLink', route('profiles.update', $profile->id))

@section('coffeeLink', route('coffees', $profile->id))

@section('content')
    <h3>{{ $profile->username }}'s Profile</h3>

    {!! Form::model($profile, ['method' => 'PATCH','route' => ['profiles.update', $profile->id]]) !!}
    <div class="row">
        <div class="col">
            <div class="form-group">
                <strong>Username:</strong>
                {!! Form::text('username', null, ['placeholder' => 'Username','class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <strong>Full name:</strong>
                {!! Form::text('fullname', null, ['placeholder' => 'Full name','class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg float-right">Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection