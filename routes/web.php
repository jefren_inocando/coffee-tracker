<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', 'profiles');

Route::resource('profiles', 'ProfileController');

Route::get('coffees/{profile}', 'CoffeeController@show')->name('coffees');

Route::get('coffees/create/{profile}', 'CoffeeController@create')->name('coffees.create');
